# Environment

## C# for VsCode vs Visual Studio (Neither zero nor one ;-) )

C# Dev Kit : https://devblogs.microsoft.com/visualstudio/announcing-csharp-dev-kit-for-visual-studio-code/

Documentation : https://code.visualstudio.com/docs/csharp/get-started

### Pros Vs Code

Free individuals...

Isolates and share development environment

IA Autocomplete and Refactor 

### Cons Vs Code

Refactor tools.

"Inconsistent" Shortcuts (Conditional).

Breaks with vscode updates (I had to downgrade v1.81).

Needs to manually build to discover new tests before running them

## Visual Studio Live Share

* Simultaneous Read Write
* Focus
* Comments
* Web or VsCode or Visual Studio

## Visual Studio Code Useful Commands Shortcuts

General :

* Zoom In / Out : Ctrl + / -
* Toggle Explorer : Ctrl + B
* Toggle Terminal : Ctrl + J

Tabs :

* Next/Previous Tab (Group independendt) => Ctrl + AvPag / Repag
* Last Recent Tab In Group => Ctrl + Tab

Groups :

* Toggle Zen Mode : Ctrl + K   Z
* Maximize Group : Ctrl K  M
* Split Vertically groups : Ctrl + º
* Split horizontally groups : Ctrl +  K    Ctrl +  º
* Split Editor inside a Group  Ctrl + K   Ctrl + Shift + º

# **TDD**

## **Transformation Priorirty Premise Guidelines.**

* **({}–>nil)** no code at all->code that employs nil
* **(nil->constant)**
* **(constant->constant+)** a simple constant to a more complex constant
* **(constant->scalar)** replacing a constant with a variable or an argument
* **(statement->statements)** adding more unconditional statements.
* **(unconditional->if)** splitting the execution path
* **(scalar->array)**
* **(array->container)**
* **sstatement->recursion**
* **(if->while)**
* **(expression->function)** replacing an expression with a function or algorithm
* **(variable->assignment)** replacing the value of a variable.*

## Detect Responsabilities to simplify TDD

Identify responsibilities as early as possible to apply the principle of single responsibility. This will require the creation of several units to be tested.

## Codurance Katas

Mines Weeper

https://www.codurance.com/katas
