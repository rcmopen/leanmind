FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build

WORKDIR /leanmind

COPY . .

RUN dotnet restore

RUN dotnet build -c Release

WORKDIR /leanmind/publish

RUN dotnet publish /leanmind/LeanMind/LeanMind.csproj -c Release -o out


FROM mcr.microsoft.com/dotnet/aspnet:6.0 as runtime

EXPOSE ${HTTP_PORT}

VOLUME /leanmind_host

WORKDIR /leanmind

COPY ./script.sh .

RUN chmod +x script.sh


COPY --from=build /leanmind/publish/out .

ENTRYPOINT ["/leanmind/script.sh"]