﻿using LeanMind.AbstractTemplates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeanMind.Implementations.Counters
{
    public class RecursiveMultiplicationCyclesCounter : MultiplicationCyclesCounter
    {
        public RecursiveMultiplicationCyclesCounter(DigitMultiplier multiplier) : base(multiplier)
        {
        }

        public override int Count(int number)
        {
            number = Math.Abs(number);

            var product = multiplier.Multiply(number);
            if (product == DigitMultiplier.SingleDigitNumber)
				return 0;
            else
				return 1 + Count(product);
        }
    }
}
