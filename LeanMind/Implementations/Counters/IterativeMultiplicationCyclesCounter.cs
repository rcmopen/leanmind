﻿using LeanMind.AbstractTemplates;
using LeanMind.Implementations.Multipliers;

namespace LeanMind.Implementations.Counters
{
    public class IterativeMultiplicationCyclesCounter : MultiplicationCyclesCounter
    {
        public IterativeMultiplicationCyclesCounter(DigitMultiplier multiplier) : base(multiplier)
        {
        }
        public override int Count(int number)
        {
            int cycles = 0;
            int nextNumber = number;
            while ((nextNumber = multiplier.Multiply(nextNumber)) != DigitMultiplier.SingleDigitNumber)
            {
                cycles++;
            }

            return cycles;
        }
    }
}
