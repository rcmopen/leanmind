﻿using LeanMind.AbstractTemplates;
using System;

namespace LeanMind.Implementations.Multipliers
{
    public class MathDigitMultiplier : DigitMultiplier
    {
        public override int Multiply(int number)
        {
			number = Math.Abs(number);
			
            var product = 1;
            var leftMostDigits = number;
			var rightMostDigit = leftMostDigits % 10;

			bool isSingleDigitNumber = (rightMostDigit == leftMostDigits);

            if (isSingleDigitNumber)
				return SingleDigitNumber;
			do
			{
                product = product * rightMostDigit;

                leftMostDigits = leftMostDigits / 10;

				rightMostDigit = leftMostDigits % 10;

                isSingleDigitNumber = (rightMostDigit == leftMostDigits);
			}
            while (!isSingleDigitNumber);

            product = product * leftMostDigits;

            return product;
        }
    }
}
