﻿using LeanMind.AbstractTemplates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeanMind.Implementations.Multipliers
{
    public class StringDigitMultiplier : DigitMultiplier
    {
        public override int Multiply(int number)
        {
            var product = 1;
            var numberAsString = number.ToString();
            var digits = numberAsString.Select(digit => int.Parse(digit.ToString()));
            var isSingleDigitNumber= (digits.Count() == 1);

            if (isSingleDigitNumber)
                return SingleDigitNumber;
            else
            {
                foreach (var digit in digits)
                {
                    product *= digit;
                }

                return product;
            }
        }
    }
}
