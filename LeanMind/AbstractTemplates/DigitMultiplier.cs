﻿namespace LeanMind.AbstractTemplates
{
	public abstract class DigitMultiplier
    {
		public const int SingleDigitNumber = 0;
		public abstract int Multiply(int number);

    }
}
