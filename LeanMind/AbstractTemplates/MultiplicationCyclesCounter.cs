﻿namespace LeanMind.AbstractTemplates
{
	public abstract class MultiplicationCyclesCounter
	{
		protected readonly DigitMultiplier multiplier;

		public MultiplicationCyclesCounter(DigitMultiplier multiplier)
		{
			this.multiplier = multiplier;
		}
		public abstract int Count(int number);
	}
}
