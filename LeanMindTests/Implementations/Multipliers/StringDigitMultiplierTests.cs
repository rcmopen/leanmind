﻿using LeanMind.AbstractTemplates;
using LeanMind.Implementations.Multipliers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeanMindTests.Implementations.Multipliers
{
	public class StringDigitMultiplierTests

	{
		[TestCase(0, DigitMultiplier.SingleDigitNumber)]
		[TestCase(1, DigitMultiplier.SingleDigitNumber)]
		[TestCase(2, DigitMultiplier.SingleDigitNumber)]
		[TestCase(3, DigitMultiplier.SingleDigitNumber)]
		[TestCase(4, DigitMultiplier.SingleDigitNumber)]
		[TestCase(5, DigitMultiplier.SingleDigitNumber)]
		[TestCase(6, DigitMultiplier.SingleDigitNumber)]
		[TestCase(7, DigitMultiplier.SingleDigitNumber)]
		[TestCase(8, DigitMultiplier.SingleDigitNumber)]
		[TestCase(9, DigitMultiplier.SingleDigitNumber)]
		[TestCase(12, 2)]
		[TestCase(123, 6)]
		[TestCase(99, 81)]
		[TestCase(999, 729)]
		[TestCase(12345, 120)]
		[TestCase(123456, 720)]
		public void StringMultiplierReturnsRightProduct(int inputValue, int expected)
		{
			var multiplier = new StringDigitMultiplier();

			var product = multiplier.Multiply(inputValue);

			Assert.That(product, Is.EqualTo(expected));
		}
	}
}
