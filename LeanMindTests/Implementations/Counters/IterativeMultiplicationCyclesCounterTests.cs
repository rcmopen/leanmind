﻿using LeanMind.Implementations.Counters;
using LeanMind.Implementations.Multipliers;

namespace LeanMindTests.Implementations.Counters
{
	public class IterativeMultiplicationCyclesCounterTests
	{
		[TestCase(1, 0)]
		[TestCase(2, 0)]
		[TestCase(-5, 0)]
		[TestCase(11, 1)]
		[TestCase(99, 2)]
		[TestCase(321, 1)]
		[TestCase(111111, 1)]
		[TestCase(999, 4)]
		[TestCase(1234, 2)]
		public void IterativeReturnsRightCycles(int inputValue, int expected)
		{
			var multiplier = new MathDigitMultiplier();

			var counter = new IterativeMultiplicationCyclesCounter(multiplier);
			var cycles = counter.Count(inputValue);

			Assert.That(cycles, Is.EqualTo(expected));
		}
	}
}