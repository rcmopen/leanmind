# LeanMind Test

## Description

This is a kata implemented as a technical test to test TDD 

* **Input** : An integer number with several digits
* **Output** : Number of cycles needed to reduce the original number to a single-digit number by multiplying its digits

Examples :

* Given number 99, **two** cycles are needed. They are built like this :
  1. **99**  => 9 * 9 = 81 (**two digits, continue**)
  2. **81** => 8 * 1 = 8 (**one digit, end!**)
* Given number 12345, **two** cycles are needed. They are built like this :
  1. **1234** => 1 * 2 * 3 * 4 = 24 (**two digits, continue**)
  2. **24** => 2 * 4 = 8 (**one digit, end!**)

## Clone in a Visual Studio Code Volume of a Dev Container)

* Install Visual Studio Code
* Install Dev Containers extension
* Click [here](https://vscode.dev/redirect?url=vscode://ms-vscode-remote.remote-containers/cloneInVolume?url=https://gitlab.com/rcmopen/leanmind)
