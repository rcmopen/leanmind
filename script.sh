#!/bin/bash

ls -la

mkdir -p /leanmind_host/.LeanMind.Artifacts
cp -rf * /leanmind_host/.LeanMind.Artifacts
echo "Artifacts copied to .LeanMind.Artifacts"

dotnet LeanMind.dll
