docker build -t leanmind . --no-cache
SET HTTP_PORT=80
SET ASPNETCORE_URLS=http://+:%HTTP_PORT%
docker run -t -i -p %HTTP_PORT%:%HTTP_PORT% --rm  -v .:/leanmind_host  --env-file build_artifacts_and_run.env leanmind
